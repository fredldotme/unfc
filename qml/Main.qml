/*
 * Copyright (C) 2020 Daniel Frost
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'unfc.danfro'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    // property var modems :[]
    // property bool firstRun: true

    // Settings {
    //     id: settings
    //     property bool firstrun: root.firstRun
    // }

    PageStack {
        id: pageStack
        Component.onCompleted: push(Qt.resolvedUrl("NfcMessages.qml"));
    }
}
